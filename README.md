http://git.oschina.net/clmao/haolinkbyyii
=======
## 在前面
本程序采用Yii 1.14开发，是一个导航网站。

## 使用方法：<br/>
首先导入SQL文件test.sql，<br/>
然后修改文件\protected\config\main.php，配置你的数据库信息，<br/>
运行index.php文件。<br/>
后台路径：localhost/index.php?r=admin/login/index  帐户：admin,密码：admin,后台CURD直接用GII生成的 。<br/>

##预览图<br/>
![1](http://git.oschina.net/uploads/images/2014/1009/102751_1d1094b8_106233.jpeg)<br/>

## 联系方式
有问题请留言我的<a href="http://blog.clmao.com/?page_id=145" target="_blank">博客</a>
