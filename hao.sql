-- phpMyAdmin SQL Dump
-- version 3.5.3
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 10 月 07 日 17:04
-- 服务器版本: 5.5.20
-- PHP 版本: 5.3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `hao`
--

-- --------------------------------------------------------

--
-- 表的结构 `hao_category`
--

CREATE TABLE IF NOT EXISTS `hao_category` (
  `id` smallint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '分类名称',
  `pid` smallint(10) unsigned NOT NULL DEFAULT '0' COMMENT '父级分类',
  `level` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '分类等级',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '可见状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `hao_category`
--

INSERT INTO `hao_category` (`id`, `name`, `pid`, `level`, `status`) VALUES
(1, 'PHP论坛', 0, 1, 1),
(2, 'PHP免费视频教程', 0, 1, 1),
(3, 'PHP电子书下载', 0, 1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `hao_content`
--

CREATE TABLE IF NOT EXISTS `hao_content` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(30) NOT NULL COMMENT '网站名称',
  `is_hot` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否为热门,1代表是,0代表否,默认为0.',
  `cid` smallint(5) unsigned NOT NULL COMMENT '所属分类',
  `src` varchar(200) NOT NULL COMMENT '网址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `hao_content`
--

INSERT INTO `hao_content` (`id`, `name`, `is_hot`, `cid`, `src`) VALUES
(1, '自学IT网', 0, 1, 'http://www.zixue.it'),
(2, '后盾网', 0, 1, 'http://bbs.houdunwang.com'),
(3, '脚本之家', 0, 3, 'http://www.jb51.net/books/list16_1.html'),
(4, '兄弟连', 0, 1, 'http://bbs.lampbrother.net/'),
(5, '传智播客', 0, 1, 'http://bbs.itcast.cn/forum.php'),
(6, '传智教程', 0, 2, 'http://www.itcast.cn/channel/video.shtml'),
(7, '兄弟连教程', 0, 2, 'http://www.lampbrother.net/php/html/PHPvideo/'),
(8, '18哥教程', 1, 2, 'http://www.zixue.it/PHP/'),
(9, 'PHP100', 0, 1, 'http://bbs.php100.com/'),
(10, 'IT资讯', 0, 3, 'http://download.chinaitlab.com/special/phpebook.htm'),
(11, '酷源码', 0, 3, 'http://www.kyuanma.com/soft/150/152/');

-- --------------------------------------------------------

--
-- 表的结构 `hao_option`
--

CREATE TABLE IF NOT EXISTS `hao_option` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `k` varchar(20) NOT NULL COMMENT '键名',
  `val` varchar(255) NOT NULL COMMENT '键值',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `hao_option`
--

INSERT INTO `hao_option` (`id`, `k`, `val`) VALUES
(1, 'site_name', 'PHP网址大全'),
(2, 'site_title', '为PHP程序猿提供学习、交流的好网址！'),
(3, 'site_ad', '本站采用YII 1.14开发，源码开放，可任意下载。');

-- --------------------------------------------------------

--
-- 表的结构 `hao_user`
--

CREATE TABLE IF NOT EXISTS `hao_user` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(20) NOT NULL COMMENT '用户名',
  `pwd` char(32) NOT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `hao_user`
--

INSERT INTO `hao_user` (`id`, `name`, `pwd`) VALUES
(1, 'admin', 'cd2fab770d4a7bc67de4835c2e3ac529');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
