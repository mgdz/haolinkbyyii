<?php

class HomeController extends Controller {

    public $layout = '/layouts/my';
    public $pageDes = '';
    public $site_ad = '';
    public $site_title = '';

    public function filters() {
        return array(
            array(
                'system.web.widgets.COutputCache + index',
                'duration' => 3600,
                'dependency' => array(
                    'class' => 'CDbCacheDependency',
                    'sql' => 'SELECT count(*) FROM {{content}}',
                )
            ),
        );
    }

    public function init() {
        parent::init();
        $this->site_ad = Option::model()->get_option('site_ad');
        $this->site_title = Option::model()->get_option('site_title');
    }

    public function actionIndex() {
        $data = Content::model()->get_all();
        $this->render('index', array('data' => $data));
    }

}
