<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="Description" content="<?php echo CHtml::encode($this->pageDes); ?>">
<meta name="author" content="Clmao">
<base target="_blank">
<meta http-equiv="Cache-Control" content="no-transform" />
<meta http-equiv="Cache-Control" content="no-siteapp"/>
<meta http-equiv="Window-target" content="_top">
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl ?>/css/zui.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl ?>/css/example.css">
<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl ?>/images/favicon.ico">
</head>

<body>
<div id="maincontain">
  <div id='headTitle'>
    <div class="wrapper">
        <div id='siteLogo'> <a href='<?php echo $this->createUrl('/');?>' target="_self" ><img src="<?php echo Yii::app()->request->baseUrl ?>/images/logo.png"></a> </div>
      <div id='siteSlogan'><span><?php echo $this->site_title;?></span></div>
    </div>
  </div>
  <article class="article-small">
    <section class="page-section">
      <p> <a  href="http://blog.clmao.com"  class="btn btn-success"><i  class="icon-user"></i> 作者博客</a> <a  href="http://git.oschina.net/clmao/ClmaoBlog"  class="btn btn-primary"><i  class="icon-download-alt"></i> 源码下载</a> <font class='text-primary'>&nbsp;&nbsp;<?php echo $this->site_ad;?></font></p>
      <div contenteditable="false" spellcheck="false" class="segment no-padding"> <br>

	<?php echo $content; ?>
      </div>
<center>
       
        </cernter>
        <center>
        Power By Clmao
        </cernter>
    </section>
  </article>
</div>
</body>
</html>

