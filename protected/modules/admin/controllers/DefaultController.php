<?php

class DefaultController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * 验证规则.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
           
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function init() {
        parent::init();
    }

    public function actionIndex() {
        $this->renderPartial('index');
    }

    public function actionHeader() {
        $this->renderPartial('header');
    }

    public function actionMenu() {
        $this->renderPartial('menu');
    }

    public function actionMain() {
        $this->renderPartial('main');
    }

}
