<?php

class LoginController extends XAdminiBase {


    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
                'maxLength' => '4',
                'minLength' => '4',
            ),
            array('allow',
                'actions' => array('captcha'),
                'users' => array('*'),
            ),
        );
    }

    //登陆验证
    public function actionIndex() {
        $model = new LoginForm;
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];

            if ($model->validate() && $model->login()) {
                Yii::app()->session['logintime'] = time();
                $this->redirect(array('/admin/default/index'));
            }
        }
        $this->renderPartial('index', array('model' => $model));
    }

    public function actionLogoout() {
        Yii::app()->user->logout();
        $this->redirect($this->createUrl('login/index'));
    }

}
