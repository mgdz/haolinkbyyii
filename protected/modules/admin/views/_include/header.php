<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Clmao">
<meta http-equiv="Cache-Control" content="no-transform" />
<meta http-equiv="Cache-Control" content="no-siteapp"/>
<meta http-equiv="Window-target" content="_top">
<title><?php echo $this->pageTitle='登陆 | ' . $this->site_name ;?></title>
<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl ?>/images/favicon.ico">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl ?>/css/zui.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl ?>/css/example.css">
<script src="<?php echo Yii::app()->request->baseUrl ?>/js/jquery.js"></script>
</head>

<body>
<div id="maincontain">
  <div id='headTitle'>
    <div class="wrapper">
      <div id='siteLogo'> <a href='/' ><img src="<?php echo Yii::app()->request->baseUrl ?>/images/logo.png"></a> </div>
      <div id='siteSlogan'><span>为PHP程序猿提供学习、交流的好网址！</span></div>
    </div>
  </div>
  <article class="article-small">
    <section class="page-section">
      <p> <a  target="_blank"  href="http://blog.clmao.com"  class="btn btn-success"><i  class="icon-user"></i> 作者博客</a> <a  target="_blank"  href="http://git.oschina.net/clmao/ClmaoBlog"  class="btn btn-primary"><i  class="icon-download-alt"></i> 源码下载</a> <font class='text-primary'>&nbsp;&nbsp;本站采用THINKPHP + ZUI开发而成，后台功能齐全，欢迎大家下载学习使用</font></p>
      <div contenteditable="false" spellcheck="false" class="segment no-padding"> <br>
        
        <div class="list">