<!DOCTYPE HTML PUBLIC "-//W3C//Dtd HTML 4.0 Transitional//EN">
<HTML><HEAD>
<META http-equiv=Content-Type content="text/html; charset=utf-8">
<LINK href="<?php echo Yii::app()->request->baseUrl ?>/css/admin.css" type="text/css" rel="stylesheet">
<SCRIPT language=javascript>
	function expand(el)
	{
		childObj = document.getElementById("child" + el);

		if (childObj.style.display == 'none')
		{
			childObj.style.display = 'block';
		}
		else
		{
			childObj.style.display = 'none';
		}
		return;
	}
</SCRIPT>
</HEAD>
<BODY>
<table height="100%" cellSpacing=0 cellPadding=0 width=170 
background=<?php echo Yii::app()->request->baseUrl ?>/images/menu_bg.jpg border=0>
  <tr>
    <td vAlign=top align=middle>
      <table cellSpacing=0 cellPadding=0 width="100%" border=0>
        
        <tr>
          <td height=10></td></tr></table>
      <table cellSpacing=0 cellPadding=0 width=150 border=0>
        
        <tr height=22>
          <td style="PADDING-LEFT: 30px" background=<?php echo Yii::app()->request->baseUrl ?>/images/menu_bt.jpg><A 
            class=menuParent onclick=expand(1) 
            href="javascript:void(0);">帐户管理</A></td></tr>
        <tr height=4>
          <td></td></tr></table>
      <table id=child1 style="DISPLAY: none" cellSpacing=0 cellPadding=0 
      width=150 border=0>
        <tr height=20>
          <td align=middle width=30><IMG height=9 
            src="<?php echo Yii::app()->request->baseUrl ?>/images/menu_icon.gif" width=9></td>
          <td><A class=menuChild 
            href="<?php echo $this->createUrl('user/admin');?>" 
            target=main>查看帐户</A></td></tr>
        <tr height=20>
          <td align=middle width=30><IMG height=9 
            src="<?php echo Yii::app()->request->baseUrl ?>/images/menu_icon.gif" width=9></td>
          <td><A class=menuChild 
            href="<?php echo $this->createUrl('user/create');?>" 
            target=main>添加帐户</A></td></tr>
      
        <tr height=4>
          <td colSpan=2></td></tr></table>
      <table cellSpacing=0 cellPadding=0 width=150 border=0>
        <tr height=22>
          <td style="PADDING-LEFT: 30px" background=images/menu_bt.jpg><A 
            class=menuParent onclick=expand(2) 
            href="javascript:void(0);">内容管理</A></td></tr>
        <tr height=4>
          <td></td></tr></table>
      <table id=child2 style="DISPLAY: none" cellSpacing=0 cellPadding=0 
      width=150 border=0>
        <tr height=20>
          <td align=middle width=30><IMG height=9 
            src="<?php echo Yii::app()->request->baseUrl ?>/images/menu_icon.gif" width=9></td>
          <td><A class=menuChild 
            href="<?php echo $this->createUrl('content/create');?>" 
            target=main>添加链接</A></td></tr>
        <tr height=20>
          <td align=middle width=30><IMG height=9 
            src="<?php echo Yii::app()->request->baseUrl ?>/images/menu_icon.gif" width=9></td>
          <td><A class=menuChild 
            href="<?php echo $this->createUrl('content/admin');?>" 
            target=main>查看链接</A></td></tr>
        <tr height=20>
          <td align=middle width=30><IMG height=9 
            src="<?php echo Yii::app()->request->baseUrl ?>/images/menu_icon.gif" width=9></td>
          <td><A class=menuChild 
            href="<?php echo $this->createUrl('category/create');?>" 
            target=main>添加分类</A></td></tr>
        
        <tr height=20>
          <td align=middle width=30><IMG height=9 
            src="<?php echo Yii::app()->request->baseUrl ?>/images/menu_icon.gif" width=9></td>
          <td><A class=menuChild 
            href="<?php echo $this->createUrl('category/admin');?>" 
            target=main>查看分类</A>
          </td>
        </tr>
        
         <tr height=4>
          <td colSpan=2></td></tr></table>
        
    <table cellSpacing=0 cellPadding=0 width=150 border=0>
        
        <tr height=22>
          <td style="PADDING-LEFT: 30px" background=<?php echo Yii::app()->request->baseUrl ?>/images/menu_bt.jpg><A 
            class=menuParent onclick=expand(3) 
            href="javascript:void(0);">碎片管理</A></td></tr>
        <tr height=4>
          <td></td>
        </tr>
    </table>
      <table id=child3 style="DISPLAY: none" cellSpacing=0 cellPadding=0 
      width=150 border=0>
          <tr height=20>
          <td align=middle width=30><IMG height=9 
            src="<?php echo Yii::app()->request->baseUrl ?>/images/menu_icon.gif" width=9></td>
          <td><A class=menuChild 
            href="<?php echo $this->createUrl('option/create');?>" 
            target=main>添加碎片</A>
          </td>
        </tr>
          
        <tr height=20>
          <td align=middle width=30><IMG height=9 
            src="<?php echo Yii::app()->request->baseUrl ?>/images/menu_icon.gif" width=9></td>
          <td><A class=menuChild 
            href="<?php echo $this->createUrl('option/admin');?>" 
            target=main>查看碎片</A>
          </td>
        </tr>
        
      
        <tr height=4>
          <td colSpan=2></td>
        </tr>
      </table>     
        
        
        
        
      
    <table cellSpacing=0 cellPadding=0 width=150 border=0>
        
        <tr height=22>
          <td style="PADDING-LEFT: 30px" background=<?php echo Yii::app()->request->baseUrl ?>/images/menu_bt.jpg><A 
            class=menuParent onclick=expand(10) 
            href="javascript:void(0);">其他管理</A></td></tr>
        <tr height=4>
          <td></td>
        </tr>
    </table>
      <table id=child10 style="DISPLAY: none" cellSpacing=0 cellPadding=0 
      width=150 border=0>
        <tr height=20>
          <td align=middle width=30><IMG height=9 
            src="<?php echo Yii::app()->request->baseUrl ?>/images/menu_icon.gif" width=9></td>
          <td><A class=menuChild 
            href="<?php echo $this->createUrl('help/clear');?>" 
            target=main>清理缓存</A></td>
        </tr>
        
      
        <tr height=4>
          <td colSpan=2></td>
        </tr>
      </table> 
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
      
      
      
     
      
      
      
      
      </td>
    <td width=1 bgColor=#d1e6f7></td></tr></table></BODY></HTML>
